// https://en.wikipedia.org/wiki/Block_Elements
// https://en.wikipedia.org/wiki/Box_Drawing
// https://en.wikipedia.org/wiki/List_of_Unicode_characters
// get an html-pdf creator from node and build my cv in text, html and pdf.
// save all to yaml to easily change data.

let fs = require('fs');
let yaml = require('yaml');
let _ = require('underscore');
let connect = require('connect');
let serveStatic = require('serve-static');
let clear = require('clear');
const PDFDocument = require('pdfkit');

let symbols = require('./src/symbols');
let helpers = require('./src/helpers');
let itemsChart = require('./src/itemsChart');
let boxedTitle = require('./src/boxedTitle').boxedTitle;
let textLine = require('./src/textLine').textLine;
let dataColumns = require('./src/dataColumns').dataColumns;
let logo = require('./src/logo').logo;
let CircleGraph = require('./src/circleGraph').CircleGraph;
let dump = require('dumper').dumper;
let getSizeChart = itemsChart.getSizeChart;

let output = [];


clear();

let switches = {
	personal:true,
	languages:true,
	frameworks:false,
	other:true,
	circle:false,
    columns:true,
    overview:true, 
    notes:true,
}

function addToOutput(str)
{
    output.push(str);
}

function calcMyAge() 
{ 
    dob = new Date(1976,09,10);
    var diff_ms = Date.now() - dob.getTime();
    var age_dt = new Date(diff_ms); 
  
    return Math.abs(age_dt.getUTCFullYear() - 1970);
}

/**
 * Personal details
 */
if (switches.personal){
    addToOutput(boxedTitle("Personal Details"));
    addToOutput(textLine("Rodrigo Dias"));
    addToOutput(textLine("Flat c, 9 Exmouth Street, E1 0PH"));
    addToOutput(textLine("rodrigo.pitta@gmail.com"));
    addToOutput(textLine("10th Sep 1976 ("+calcMyAge()+"), Portugal"));
    addToOutput(textLine(""));
}


/**
 * Languages
 */
if(switches.languages){
    addToOutput(boxedTitle("programming languages"));
    var data = {
        PHP: 100,
        Python: 70,
        Golang: 65,
        ES6: 60,
        Elixir: 30,
        Dart: 20,
    };
    addToOutput(getSizeChart(data));
}


/**
 * Frameworks
 */
if (switches.frameworks){
    addToOutput(boxedTitle("Preferred PHP Frameworks"));
    var data = {
        "Symfony 4": 100,
        "Laravel 5": 90,
        CodeIgniter: 70,
        Wordpress: 70,
        Yii: 60,
        "Zend 1": 50,
    };
    addToOutput(getSizeChart(data));
}

/**
 * Other stuff
 */
if(switches.other){
    addToOutput(boxedTitle("Other Knowledge"));
    var data = {
        Docker: 80,
        "PHPUnit/Behat":70,
        TDD:70,
        "C#": 50,
        "CI/CD": 40,
        git: 40,
        Jquery: 35,
        "Unix Shell": 30,
        "Node.js" : 30,
        Sass:10,
    };
    addToOutput(getSizeChart(data));
}


/**
 * Pie Chart
 */
// MongoDB, Postgres, mysql, elasticsearch, RST, Redis
// github languages
if(switches.circle){
    var data = {radius:10, data:[{javascript:25, sql:25, php:25, others:25}] };
	var c = new CircleGraph(data);
	addToOutput(c.draw());
}

/**
 * Columns Data
 */
if(switches.columns){
    addToOutput(boxedTitle("Work Experience"));
    var data = [
        ['Title', 'Company', 'Location', 'Dates'],
        ['Tech Lead', 'Avanti Communications', 'London', 'Mar 2019 - Present'],
        ['Senior PHP Developer', 'Browser', 'London', 'Oct 2017 - Mar 2019'],
        ['Senior PHP Developer', 'VGD', 'London', 'Mar 2016 - Aug 2017'],
        ['Travel & Study Gap', 'UK', 'London', 'Sep 2015 - Feb 2016'],
        ['PHP Developer', 'Liberty Tech', 'London', 'Mar 2014 - Aug 2015'],
        ['PHP Developer', 'Freelance', 'London', 'Aug 2013 - Mar 2014'],
        ['PHP Developer', 'Premier IT', 'London', 'Jul 2012 - Aug 2013'],
        ['PHP Developer','Pilotbean','London','Nov 2011 - Apr 2012'],
        ['PHP Developer', 'Helastel', 'Bristol', 'Mar 2011 - Sep 2011'],
        ['PHP Developer', 'CabinetUK', 'London', 'Jul 2008 - Aug 2010'],
        ['PHP Developer', 'Markettiers 4DC', 'London', 'Jan 2008 - May 2008'],
        ['PHP Developer', 'Migs On', 'London', 'Nov 2007 - Jan 2008'],
        ['Director', 'Frontspace', 'Portugal', 'Apr 2005 - Jul 2007'],
        ['3D Modeller', 'Fullscreen', 'Portugal', 'Jun 2000 - Apr 2004'],
    ];

    addToOutput(dataColumns(data));
    
    // For PDF not to break inside a box
    addToOutput(textLine(""));
}

if (switches.overview){
    addToOutput(boxedTitle("Overview"));
    addToOutput(textLine("Team Lead & scrum-master for a team of 4 devs. Many times also doing P.O. position. Preference for 100% BDD/TDD projects using Symfony 4 and responsible for bi-weekly sprints, standups, demos, retros and planning, all of this within a 100% waterfall organisation.", true))
    addToOutput(textLine("Worked on Backend CMS (craftCMS) and Golang API serve a react frontent. Migrated 20,000 thousand articles and users from a legacy system in ASP and SQLServer and also multiple small golang workers to facilitate realtime processing of newsdesk application with AWS SQS messages and Elasticsearch DB.", true))
    addToOutput(textLine("In my agency work, I had a variety of projects that include PHP frameworks such as Symfony 3.4, Laravel 5.5 and all sorts of other more obscure ones (like mod-x)", true));
    addToOutput(textLine("Responsible for bringing innovation to many of the workplaces where I worked, such as Git in LibertyTech, Bash Shell script automations for SVN at PremierIT and a soap API at CabinetUK.", true));
    addToOutput(textLine("Keen user of backlogs and todo lists", true));
    addToOutput(textLine("Really thinks that Scrum is the new Waterfall (I can explain why :) ).", true));
    addToOutput(textLine("Works best in the mornings", true));
    addToOutput(textLine("\n"));
}


if (switches.notes){
    addToOutput(boxedTitle("Personal Details"));
    addToOutput(textLine("I'm used to building smart applications involving multiple components, API's and third-party connections. I'm not very good at css, and I'm slower than your average developer. I have a thing for learning new programming languages and I'm addicted to programming gurus, from Kent Beck, Martin Fowler to Uncle Bob and Alistair Cockburn and for the most part, obcessed with teamwork and efficiency", true));
    addToOutput(textLine("The easiest way to catch me is via my email (rodrigo.pitta@gmail.com) as my phone is always muted", true));
    addToOutput(textLine("I do have a website (rodderscode.co.uk) and an interactive CV that is a work in progress but I was told it adds a lot to this one. Please check http://cv.rodderscode.co.uk", true));
    addToOutput(textLine("This CV was purposely made in ASCII so that it fits into every single upload form on the internet (and mess up recruiters standard software) and also because it looks “retro” and cool.", true));
}

//console.log(getYamlData('data.yml'));

function makePDF(){
    const doc = new PDFDocument;

    doc.pipe(fs.createWriteStream('output.pdf'));
    
    // Embed a font, set the font size, and render some text
    doc.font('fonts/PT_Mono/PTMono-Regular.ttf')
    .fontSize(9)
    .text(output.join("\n"), 50, 50);
    
    // Add an image, constrain it to a given size, and center it vertically and horizontally
    // doc.image('path/to/image.png', {
    //    fit: [250, 300],
    //    align: 'center',
    //    valign: 'center'
    // });
    
    // Add another page
    // doc.addPage()
    // .fontSize(16)
    // .text('Here is some vector graphics...', 100, 100);
    
    // Draw a triangle
    // doc.save()
    // .moveTo(100, 150)
    // .lineTo(100, 250)
    // .lineTo(200, 250)
    // .fill("#FF3300");
    
    // // Apply some transforms and render an SVG path with the 'even-odd' fill rule
    // doc.scale(0.6)
    // .translate(470, -380)
    // .path('M 250,75 L 323,301 131,161 369,161 177,301 z')
    // .fill('red', 'even-odd')
    // .restore();
    
    // // Add some text with annotations
    // doc.addPage()
    // .fillColor("blue")
    // .text('Here is a link!', 100, 100)
    // .underline(100, 100, 160, 27, {color: "#0000FF"})
    // .link(100, 100, 160, 27, 'http://google.com/');

    doc.save();
    
    // Finalize PDF file
    doc.end();
}

makePDF();

console.log(output.join("\n\r"));
