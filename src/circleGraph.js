var _ = require('underscore');
var symbols = require('./symbols');
var helpers = require('./helpers');
var itemsChart = require('./itemsChart');
var dump = require('dumper').dumper;
var Vector = require('./Vector').Vector;

var box = symbols.box();
var semiblocks = symbols.semiblocks();
var filledBlocks = symbols.filledBlocks();
var misc = symbols.misc();

var getNSymbols = helpers.getNSymbols;
var getNSpaces = helpers.getNSpaces;
var getNSymbols = helpers.getNSymbols;
var getNPSymbols = helpers.getNPSymbols;




class CircleGraph {

	constructor(data) 
	{
    	this.r = data.radius; 
		this.width = 0; 
		this.height = 0;
		this.matrix = new Map();
		this.createMatrix();
		//this.draw();
  	}

  	draw()
  	{
  		

  		for (var key of this.matrix.keys()) 
  		{
  			var dist = key.sub(this.center()).mag();
  			if (dist < 4)
  				this.matrix.set(key, {data:"X"})
  		}


  		var str = '';
  		for (var key of this.matrix.keys()) 
  		{
  			var value = this.getValue(key);

  			str += "" + value.data;

  			if(key.x >= this.width)
  				str += "\n";	
  			
  		}

  		return str;
  	}

  	draw2()
  	{
  		for(y -= radius; y<radius; y++)
		{
			half_row_width = sqrt((radius*radius)-(y*y));
			for(x -= half_row_width; x < half_row_width; x++)
				WritePixel(centre_x+x, centre_y+y, colour);
		}
  	}


	createMatrix()
	{	

		for(var y=0; y < (this.r * 2); y++){
			
			for(var x=0; x < (this.r * 4); x++){
				var id = x + "-" + y;
				this.matrix.set(new Vector(x,y), {data:' '}); 
				this.width = (this.width < x)? x : this.width;
			}
			
		}
		this.height = (this.height < y)? y : this.height;
	}


	center()
	{
		return new Vector(Math.ceil(this.width / 2), Math.ceil(this.height / 2));
	}

	getValue(key)
	{
		return this.matrix.get(this.getKey(key, this.matrix));
	}

	getKey(lookup)
	{
		for (var key of this.matrix.keys()) {
	  		if (key.x == lookup.x && key.y == lookup.y)
	  			return key;
	  	}
	  	return false; 
	}

}



exports.CircleGraph = CircleGraph; 