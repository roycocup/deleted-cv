var parseSymbol = function(symbol) {return String.fromCharCode(parseInt(symbol, 16));}
module.exports.parseSymbol = parseSymbol; 
exports.getNSpaces = function(n){ var s = ''; for(var i = 0; i < n; i++) s +=' '; return s}
exports.getNSymbols = function(n, symbol){ var s = ''; for(var i = 0; i < n; i++) s +=symbol; return s}
exports.getNPSymbols = function(n, symbol){ var s = ''; for(var i = 0; i < n; i++) s +=parseSymbol(symbol); return s}
