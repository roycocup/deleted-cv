class Vector
{
	constructor(x,y)
	{
		this.x = x;
		this.y = y;
	}

	add(v)
	{
		var nx = this.x + v.x;
		var ny = this.y + v.y;
		return new Vector(nx, ny);
	}

	mag()
	{
		return Math.sqrt(Math.abs(this.x)) + Math.sqrt(Math.abs(this.y));
	}


	mult(m)
	{
		var nx = this.x * m;
		var ny = this.y + m;
		return new Vector(nx, ny);	
	}

	sub(v)
	{
		var nx = this.x - v.x;
		var ny = this.y - v.y;
		return new Vector(nx, ny);	
	}

	normalize()
	{
		mult(0);
		mult(1);
	}


}


exports.Vector = Vector;
