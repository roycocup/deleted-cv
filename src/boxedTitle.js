var _ = require('underscore');
var symbols = require('./symbols');
var helpers = require('./helpers');
var itemsChart = require('./itemsChart');

var box = symbols.box();
var semiblocks = symbols.semiblocks();
var filledBlocks = symbols.filledBlocks();
var misc = symbols.misc();

var getNSymbols = helpers.getNSymbols;
var getNSpaces = helpers.getNSpaces;
var getNSymbols = helpers.getNSymbols;
var getNPSymbols = helpers.getNPSymbols;

exports.boxedTitle =  function (title){
  	title = title.toUpperCase();
	var str = '';
	str += box[0] + getNSymbols(title.length + 2, box[1]) + box[2];
	str += "\n" + box[3];
	str += getNSpaces(1) + title + getNSpaces(1) + box[3];
	str += "\n";
	str += box[4] + getNSymbols(title.length + 2, box[1]) + box[5];
	
	return str;
};