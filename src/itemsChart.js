// todo: order this map
let _ = require('underscore');
let symbols = require('./symbols');
let helpers = require('./helpers');
let itemsChart = require('./itemsChart');

let box = symbols.box();
let semiblocks = symbols.semiblocks();
let filledBlocks = symbols.filledBlocks();
let misc = symbols.misc();

let getNSymbols = helpers.getNSymbols;
let getNSpaces = helpers.getNSpaces;
let getNPSymbols = helpers.getNPSymbols;

exports.getSizeChart = function getSizeChart(data, incPercent = false)
{
    let str = '';
    let i=0;
    let longestWord = 0;

    _.each(data, function(v,k){
        longestWord = (k.length > longestWord)? k.length : longestWord;
    });

    _.each(data, function(v,k){
        str += getNSymbols(1, ' *') + getNSpaces(1) + k;
        str += getNSpaces( (longestWord - k.length) + 2);
        let n = Math.floor(v/10);

        if (n > 0)
            str += getNPSymbols((n>=3)?3:n, filledBlocks[0]);
        if (n > 3)
            str += getNPSymbols((n>=6)?3:n-3, filledBlocks[1]);
        if (n > 6)
            str += getNPSymbols((n>=9)?3:n-6, filledBlocks[2]);

        if (incPercent)
          str += v + "%"

        str += "\n";

    });

    return str;
}
