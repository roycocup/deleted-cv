var _ = require('underscore');
var symbols = require('./symbols');
var helpers = require('./helpers');
var itemsChart = require('./itemsChart');

var box = symbols.box();
var semiblocks = symbols.semiblocks();
var filledBlocks = symbols.filledBlocks();
var misc = symbols.misc();

var getNSymbols = helpers.getNSymbols;
var getNSpaces = helpers.getNSpaces;
var getNSymbols = helpers.getNSymbols;
var getNPSymbols = helpers.getNPSymbols;

exports.textLine = function (text, bullet = false){
    str = (bullet)? getNSpaces(2) + text : getNSpaces(2) + '* ' + text;
    str = getNSpaces(2) + text;
    return str;
};