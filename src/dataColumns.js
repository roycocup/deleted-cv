let _ = require('underscore');
let symbols = require('./symbols');
let helpers = require('./helpers');
let itemsChart = require('./itemsChart');
let dump = require('dumper').dumper;

let box = symbols.box();
let semiblocks = symbols.semiblocks();
let filledBlocks = symbols.filledBlocks();
let misc = symbols.misc();

let getNSymbols = helpers.getNSymbols;
let getNSpaces = helpers.getNSpaces;
let getNPSymbols = helpers.getNPSymbols;

let dataColumns = function(data){
	str = '';

	let numColumns = data[0].length;
	let longestWordPerColumn = [];

	let colData = [];
	// get the largestWordPerColumn & assign columndata
	let row = 0;

	_.each(data, function(item){
		for(let col = 0; col < numColumns; col++)
        {
			colData.push( {row: row, col: col, data: item[col], charCount: item[col].length } );
		}
		row++;
	});

    let columnsInfo = getInfoForColumns(colData, numColumns);

    return getFinalStr(colData, columnsInfo, row);

}


function getFinalStr(colData, columnsInfo, numRow)
{
    let str = '';
    let cellPadding = 2;
    let textMargin = 1;

    // header
    str = str + division(columnsInfo, cellPadding);
    str = str + createRow(0, colData, columnsInfo, textMargin, cellPadding, true);

    // body
    for(let row = 1; row < numRow; row++){
        str = str + createRow(row, colData, columnsInfo, textMargin, cellPadding, false);
    }

    str = str + division(columnsInfo, cellPadding);

    return str;
}


function createRow(row, colData, columnsInfo, textMargin, cellPadding, includeDivision = false)
{
    let str = "";
    str = str + misc.side;

    colData
        .filter( (e) => e.row == row )
        .map( (element) => str = str + getNSymbols(textMargin, ' ')
            + element.data
            + getNSpaces( (columnsInfo[element.col].maxChars - element.charCount) + textMargin)
            + getNSymbols(1, misc.side));

    str = str + getNSymbols(1, "\n");

    if(includeDivision)
        str = str + division(columnsInfo, cellPadding);

    return str;
}


/**
 * Creates a line division calculating the spaces that each cell must have
 * @param columnsInfo
 * @param cellPadding
 * @returns {string}
 */
function division(columnsInfo, cellPadding)
{
    let str = "";
    for(let i = 0; i < columnsInfo.length; i++){
        str = str + misc.cross;
        let n = columnsInfo
            .filter((e) => e.col == i)
            .map((e) => e.maxChars + cellPadding)
            .join();
        str = str + getNSymbols(n, misc.dash);
    }
    str = str + misc.cross + getNSymbols(1, "\n");
    return str;
}

/**
 * Will return a map with the column number and the max number of chars for that column
 * @param colData
 * @param numColumns
 * @returns {Array}
 */
function getInfoForColumns(colData, numColumns)
{
    columnsInfo = [];
    for(let i = 0; i < numColumns; i++){
        let colInfo = colData.filter((o) => o.col == i);
        let charCountList = colInfo.map((e) => e.charCount);
        let max = charCountList.reduce( (e, acc) => e > acc ? acc = e: acc );
        columnsInfo.push({col:i, maxChars: max});
    }
    return columnsInfo;
}


exports.dataColumns = dataColumns;
