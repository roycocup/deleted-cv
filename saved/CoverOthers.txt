Dear XXX, 
I know I'm not a XXX developer and although I have only worked commercially with PHP I have an very keen interest in working in other languages, and have done several courses on them (ES6, React, Java, Ruby, Python, C#, etc… ). 
I at one point started learning XXX but never got round to implement any particularly noticeable projects with it.

I hope that my experience and willingness to learn can make a good starting point for a chat about your job opening? I would be more than happy to discuss a junior role or internship while I acquire the necessary skillset.

You can perhaps have a look at my github, not just for my XXX code (for which theres only a few examples) but for the rest of coding style in other languages. 
https://github.com/roycocup

I really think an interview could clear up on whether or not I could easily be a good fit within the company and I would be ever so grateful if you could take a moment to consider my CV attached.

Looking forward to hear from you, 
Rodrigo (Rod)

